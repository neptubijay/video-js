angular.module('videojs', [])
    .controller('videojsController', function($scope, $parse) {
        videojs('my_video_1').ready(function() {
            var uxcamplayer = this;

            this.playlist([{
                name: 'first',

                sources: [{
                    src: 'https://data.uxcam.com/551537630fb5d5d07af240f6/5608f3efd5904ffa29d1d6fb/560b6b2dd5904f044041e414/screen.mp4',
                    type: 'video/mp4'
                }],
                poster: ''
            }, {
                name: 'third',

                sources: [{
                    src: 'https://data.uxcam.com/551537630fb5d5d07af240f6/5608f3efd5904ffa29d1d6fb/560b6b2dd5904f044041e414/screen.mp4',
                    type: 'video/mp4'
                }],
                poster: ''
            }, {
                name: 'fourth',

                sources: [{
                    src: 'http://media.w3.org/2010/05/bunny/movie.mp4',
                    type: 'video/mp4'
                }],
                poster: ''
            }, {
                name: 'fifth',
                sources: [{
                    src: 'http://media.w3.org/2010/05/video/movie_300.mp4',
                    type: 'video/mp4'
                }],
                poster: ''
            }]);

            this.playlist.autoadvance(0);
            this.playlistUi();

            //Initialize data for markers
            uxcamplayer.markers({
                markers: markers,
                totalGestures: totalGestures
            });

            //Initialize data for zoom in activity
            var cupoints = this;
            cupoints.cuepoints();
            $.each(markers, function(index, marker) {
                cupoints.addCuepoint({
                    namespace: "logger",
                    start: marker.time,
                    end: marker.endTime,
                    onStart: function(params) {
                        if (params.error) {
                            console.error("Error at second 0");
                        } else {
                            $('#activity' + this.start).addClass('activity');
                            // console.log("Log at second "+this.start);
                        }
                    },
                    onEnd: function(params) {
                        //                    console.log("Action ends at second 15");
                        $('#activity' + this.start).removeClass('activity');
                    }
                })
            });

            //create Duplicate progressbar below original progressbar
            var VjsProgressControl = videojs.getComponent('ProgressControl');
            videojs.TrimVideo = videojs.extend(VjsProgressControl, {
                constructor: function(player, options) {
                    VjsProgressControl.call(this, player, options);
                }
            });
            uxcamplayer.controlBar.progressControl.trimVideo = uxcamplayer.controlBar.progressControl.addChild(
                new videojs.TrimVideo(uxcamplayer, {
                    el: uxcamplayer.addChild('ClickableComponent').createEl('div', {
                        className: 'custom-vjs'
                    },{
                        role: 'button'
                    }),
                    seekBar: false, // either this
                    children: {
                        seekBar: false
                    } // or this
                })
            );

            //to know fullscreen or not
            uxcamplayer.on('fullscreenchange', function() {
                var fullscreenOrNot = uxcamplayer.isFullscreen();
                if (fullscreenOrNot) {
                    $(".vjs-tech").removeClass('screen');
                    $('.video-js').css('background-color', '#000 !important');
                    $('#marvelCss').attr('disabled', 'disabled');
                    // console.log('fullscreenchange event fired! IN');
                } else {
                    $(".vjs-tech").addClass('screen');
                    $('.video-js').css('background-color', 'transparent !important');
                    $('#marvelCss').attr('disabled', false);
                    // console.log('fullscreenchange event fired! OUT');
                }
            });

            // Create variables and new button for skipInactivity
            var controlBar,
                newElement = document.createElement('button'),
                newLink = document.createElement('a'),
                newImage = document.createElement('img');

            // Assign classes to button for name
            newElement.className = 'skip vjs-control';
            // Get control bar and insert before elements
            // Remember that getElementsByClassName() returns an array
            controlBar = document.getElementsByClassName('vjs-control-bar')[0];
            // Change the class name here to move the icon in the controlBar
            insertBeforeNode = document.getElementsByClassName('vjs-progress-control')[0];
            // Insert the icon div in proper location
            controlBar.insertBefore(newElement, insertBeforeNode);

            //skip to currentTime
            $scope.skip = function(time) {
                // uxcamplayer.pause();
                uxcamplayer.currentTime(time);
                //uxcamplayer.play();
            }

            //get to the closest index as per currentTime
            $scope.closest = function(num, arr) {
                var curr = arr[0];
                var diff = Math.abs(num - curr);
                for (var val = 0; val < arr.length; val++) {
                    var newdiff = Math.abs(num - arr[val]);
                    if (newdiff < diff) {
                        diff = newdiff;
                        curr = arr[val];
                        $scope.timeIndex = arr.indexOf(curr)
                    } else if (newdiff == 1 && diff == 1) {
                        $scope.timeIndex = arr.indexOf(arr[val]);
                    } else if (newdiff !== 1 && (newdiff == diff)) {
                        $scope.timeIndex = arr.indexOf(arr[val]);
                    }
                }
                if (num > curr) {
                    $scope.timeIndex = arr.indexOf(curr) + 1;
                }
                return $scope.timeIndex;
            }
            $scope.playertime = 0;

            //set playertime on videoPlay
            uxcamplayer.on('timeupdate', function() {
                // var the_string = 'updated.time';
                // var model = $parse(the_string);
                // console.log(uxcamplayer.currentTime());
                // model.assign($scope, parseInt(uxcamplayer.currentTime()));
                $scope.playertime = parseInt(uxcamplayer.currentTime());
                $scope.$apply();
            });

            //collection of gesture markers time
            $scope.gestureMarkerTimeArray = [];
            for (var i = 0; i < markers.length; i++) {
                $.each(markers[i].gestures, function(index, val) {
                    $scope.gestureMarkerTimeArray.push(val.time)
                })
            }
            // console.log($scope.gestureMarkerTimeArray);
            // console.log($scope.gestureMarkerTimeArray.length);

            //set copiedTimeIndex
            $scope.copyIndex = 10000000;

            //skip activity onclick function
            $('.skip').click(function() {

                var elm = document.getElementById('check-skip');
                elm.checked = !elm.checked;
                if (elm.checked) {
                    $(".skip").addClass('skip-active');
                }
                var unregister = $scope.$watch("playertime", function(newValue, oldValue) {
                    // console.log(newValue + ' newValue' + oldValue + ' oldValue')
                    if ($scope.copyIndex == $scope.gestureMarkerTimeArray.length &&
                        $scope.gestureMarkerTimeArray[$scope.copyIndex] !==
                        parseInt(uxcamplayer.duration())) {
                        // console.log('if ' + $scope.copyIndex + ' ' + $scope.gestureMarkerTimeArray.length + ' ' + $scope.gestureMarkerTimeArray[$scope.copyIndex] + ' ' + parseInt(uxcamplayer.duration()));
                        $scope.copyIndex = 10000000;
                        uxcamplayer.currentTime(parseInt(uxcamplayer.duration()));
                    } else {

                        $scope.closest(newValue, $scope.gestureMarkerTimeArray);

                        $scope.copyIndex = angular.copy($scope.closest(newValue, $scope.gestureMarkerTimeArray));
                        $scope.skip($scope.gestureMarkerTimeArray[$scope.timeIndex]);
                    }

                    if (!elm.checked) {
                        $(".skip").css("background-color", "rgba(43, 51, 63, 0.7)");
                        //unwatch the playertime
                        unregister();
                    }
                }, true);
            });
        });
    });

//marvel-device iphone5s css on Video
$(window).bind("load", function() {
    var screen = $(".vjs-tech").addClass('screen');
    var beforeScreen = $('<div class="top-bar"></div><div class="sleep"></div><div class="volume"></div><div class="camera"></div><div class="sensor"></div><div class="speaker"></div>').insertBefore('.screen');
    var afterScreen = $('<div class="home"></div><div class="bottom-bar"></div>').insertAfter('.screen');
    var lis = beforeScreen.add(screen).add(afterScreen);
    lis.wrapAll('<div class="marvel-device iphone5s silver"></div>');

    var controlBar = $(".vjs-control-bar")
    controlBar.wrapAll('<div class="controlBar-wrapper"></div>');

    $('.vjs-play-control, .vjs-volumn-menu-button, .vjs-current-time, .vjs-time-control, .vjs-duration, .vjs-volume-menu-button').wrapAll('<div class="uxc_control-bar"></div>');

});